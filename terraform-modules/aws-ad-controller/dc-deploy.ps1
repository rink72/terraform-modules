<powershell>

# This is meant to be passed to AWS as userdata so will not work correctly if run locally due to the <powershell> tags

# Set DNS
# Change default DNS server and update the default search suffix with the domain name
$ifDetails = Get-NetAdapter
Set-DnsClientServerAddress -InterfaceAlias $ifDetails.Name -ServerAddresses ('${dns_address}')
Set-DnsClient -InterfaceAlias $ifDetails.Name -ConnectionSpecificSuffix '${domain_name}'
$dnsSuffixList = Get-DnsClientGlobalSetting
$newList = @('${domain_name}') + $dnsSuffixList.SuffixSearchList
Set-DnsClientGlobalSetting -SuffixSearchList $newList


# Install AD Tools
Install-WindowsFeature AD-Domain-Services -IncludeManagementTools

# Set domain controller safe mode password and AD credentials if joining domain. Also change administrator password.
$safePassword = "${safe_mode_password}" | convertto-securestring -asplaintext -force
$adPass = "${admin_password}" | ConvertTo-SecureString -AsPlainText -Force
$adCreds = New-Object System.Management.Automation.PSCredential("${domain_name}\administrator", $adPass)
Set-LocalUser -Name Administrator -AccountNeverExpires:$true -Password $adPass -PasswordNeverExpires:$true

# Create new domain controller or new domain depending on current situation
$result = Install-ADDSDomainController -DomainName '${domain_name}' -SafeModeAdministratorPassword $safePassword -Force -Credential $adCreds -ErrorAction SilentlyContinue

if ($result.Status -eq 'Error')
{
    Write-Host "No domain found. Creating new one."
    Install-ADDSForest -DomainName '${domain_name}' -SafeModeAdministratorPassword $safePassword -Force
}

</powershell>


I


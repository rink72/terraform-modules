provider "aws" {
  region = "${var.region}"
}

data "aws_ami" "win2016_base" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.ami_name}"]
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}

data "template_file" "user_data" {
  template = "${file("${path.module}/dc-deploy.ps1")}"

  vars {
    dns_address        = "${var.dns_address}"
    safe_mode_password = "${var.safe_mode_password}"
    admin_password     = "${var.admin_password}"
    domain_name        = "${var.domain_name}"
  }
}

resource "aws_instance" "ec2_instance" {
  ami           = "${data.aws_ami.win2016_base.id}"
  instance_type = "${var.instance_type}"
  subnet_id     = "${var.subnet}"

  vpc_security_group_ids = ["${var.security_group_ids}"]

  user_data = "${data.template_file.user_data.rendered}"

  tags {
    Name = "${var.server_name}"
  }
}

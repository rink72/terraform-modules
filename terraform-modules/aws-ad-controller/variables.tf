variable "instance_type" {
  description = "Instance type to deploy"
  default     = "t2.micro"
}

variable "region" {
  description = "AWS region to deploy in to."
  default     = "us-east-1"
}

variable "ami_name" {
  description = "Name of AMI to search for."
  default     = "Windows_Server-2016-English-Full-Base*"
}

variable "subnet" {
  description = "Subnet ID to deploy EC2 Instance in to"
}

variable "dns_address" {
  description = "The default DNS server to set on the AWS instance"
}

variable "safe_mode_password" {
  description = "The domain controller safe mode password to set"
}

variable "admin_password" {
  description = "The domain administrator password to set or use to join domain"
}

variable "domain_name" {
  description = "The domain name to create or join."
}

variable "server_name" {
  description = "Name to tag the server with"
  default     = "Domain Controller"
}

variable "security_group_ids" {
  description = "Security groups to assign to the instance"
  type        = "list"
}
